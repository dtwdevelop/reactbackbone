  var CommentList = React.createClass({
  render: function() {
       var commentNodes = this.props.data.map(function (comment) {
      return (
        <Comment  author={comment.author}>
          {comment.text}
        </Comment>
      );
       });
      
    return (
      <div className="commentList">
      {commentNodes}
      </div>
    );
  }
});

var Comment = React.createClass({
     handleClick: function(e) {
    e.preventDefault();
   // alert("Test come");
   
  },
  render: function() {
      var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return (
      <div className="comment" onMouseOver={this.handleClick}>
        <h2 className="commentAuthor">
          {this.props.author}
        </h2>
        <span dangerouslySetInnerHTML={{__html: rawMarkup}} />
      </div>
    );
  }
});

var CommentForm = React.createClass({
    
   handleSubmit: function(e) {
    e.preventDefault();
    var author = React.findDOMNode(this.refs.author).value.trim();
    var text = React.findDOMNode(this.refs.text).value.trim();
    
    if (!text || !author) {
      return;
    }
    // TODO: send request to the server
    this.props.onCommentSubmit({author: author, text: text});
    React.findDOMNode(this.refs.author).value = '';
    React.findDOMNode(this.refs.text).value = '';
    return;
  },
  render: function() {
    return (
      <div className="commentForm">
        <form className="commentForm" onSubmit={this.handleSubmit}>
        <input type="text" placeholder="Your name" ref="author"  />
        <textarea  placeholder="Say something..." ref="text" ></textarea>
        <input type="submit" value="Post" />
      </form>
      </div>
    );
  }
});
  var CommentBox = React.createClass({
      
  getInitialState:function(){
      return {data:[]};
  },
   handleCommentSubmit: function(comment) {
    console.log(comment);
    var comments = this.state.data;
    var newComments = comments.concat([comment]);
    this.setState({data: newComments});
    $.post('post/comment',comment,function(data){
        
    });
  },
  loadCommentsFromServer: function() {
   $.getJSON(this.props.url,function(data){
      //console.log(rez);
      if (this.isMounted()) {
       this.setState({data:data});
       }
   }.bind(this));

},
  
  componentDidMount: function() {
    this.loadCommentsFromServer();
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },
  
  render: function() {
    return (
           
      <div className="commentBox">
      
         <CommentList data={this.state.data} />
        <CommentForm onCommentSubmit={this.handleCommentSubmit} />
      </div>
     
    );
  }
});

      React.render(
       <CommentBox url="comments.json"  pollInterval={2000} />,
        document.getElementById('content')
      );

